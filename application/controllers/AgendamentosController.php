<?php

class AgendamentosController extends Zend_Controller_Action
{

    public function init()
    {
        $this->paginas = new Application_Model_Db_Paginas();
    }

    public function indexAction()
    {
        $pagina = $this->paginas->getPagina(5);
        $this->view->pagina = $pagina;
        $this->view->titulo = $pagina->titulo2;
    }


}

