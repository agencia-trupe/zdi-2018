<?php

class ResultadosController extends Zend_Controller_Action
{

    public function init()
    {
        $this->chamadas = new Application_Model_Db_Promocoes();
        $this->paginas = new Application_Model_Db_Paginas();
    }

    public function indexAction()
    {
        $pagina = $this->paginas->getPagina(3);
        $this->view->pagina = $pagina;
        $this->view->titulo = $pagina->titulo2;

        $chamadas = $this->chamadas->fetchAllWithPhoto('t1.id=1');
        $this->view->chamada = $chamadas[0];
    }


}

