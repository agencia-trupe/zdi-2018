<?php

class Application_Model_Db_PromocoesFotos extends Zend_Db_Table
{
    protected $_name = "promocoes_fotos";
    
    /**
     * Referências
     */
    protected $_dependentTables = array('Application_Model_Db_Promocoes');
    
    protected $_referenceMap = array(
        'Application_Model_Db_Promocoes' => array(
            'columns' => 'promocao_id',
            'refTableClass' => 'Application_Model_Db_Promocoes',
            'refColumns'    => 'id'
        )
    );
}
